import {API_URL, MAIN_HEADERS} from '../constants/constants'

export const getUsers = async (success) => {
    const response = await fetch(API_URL+'users');
    const users = await response.json();
    return success(users)
};

export const createUser = async (userData, success) => {
    const response = await fetch(API_URL+'users',{
        method: 'POST',
        headers: MAIN_HEADERS,
        body: JSON.stringify(userData)
    });
    return await response.json();
};

export const removeUser = async (userId, success) => {
    return await fetch(API_URL+'users/'+userId, {
        method: 'DELETE',
        headers: MAIN_HEADERS
    });
};

export const getUser = async (userId, success) => {
    const response = await fetch(API_URL+'/users/'+userId);
    const userInfo = await response.json();
    return success(userInfo);
};
