import React from "react";

const successStyle = {
    backgroundColor: 'green'
};

const warningStyle = {
    backgroundColor: 'yellow'
};

const Button = ({handleClick, styleType, text}) => {
    return (
        <button onClick={handleClick} style={styleType === 'btn-success' ? successStyle : warningStyle}>
            {text}
        </button>
    );
};

export default Button;
