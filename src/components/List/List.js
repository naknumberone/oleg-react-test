import React from "react";
import './List.sass'
import Button from "../Button/Button";
import {Link} from "react-router-dom";

const List = ({items, emptyText, handleRemove}) => {
    return (
        <ul className={'list__ul'}>
            {items.length ?
                items.map(
                    item =>
                        <li className={'list__li'} key={item._id}>
                            Пользователь: <Link to={`/users/${item._id}`}>{item.fullName}</Link> <br />
                            Email: {item.email} <br />
                            Адрес: {item.address} <br />
                        <Button handleClick={()=>handleRemove(item._id)} text={'Удалить'} />
                        </li>
                ) : emptyText}
        </ul>
    );
};

export default List;
