import React from "react";

const UserForm = ({handleSubmit, buttonText}) => {
    return (
        <form onSubmit={handleSubmit}>
            <input name="fullName" type="text" />
            <input name="email" type="email" />
            <input name="address" type="text" />
            <button type="submit">{buttonText}</button>
        </form>
    );
};

export default UserForm;
