import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Route, Switch} from 'react-router-dom';
import Users from "./views/Users/Users";
import Tasks from "./views/Tasks/Tasks";
import UserPage from "./views/UserPage/UserPage";

const App = () => {
  return (
    <div className="App">
      <Switch>
        <Route exact path={'/users'} component={Users} />
        <Route exact path={'/users/:id'} component={UserPage} />
        <Route exact path={'/tasks'} component={Tasks} />
      </Switch>
    </div>
  );
};

export default App;
