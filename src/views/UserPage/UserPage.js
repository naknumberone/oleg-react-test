import React, {useEffect, useState} from 'react';
import {getUser} from "../../services/UserService";

const UserPage = ({match}) => {
    const userId = match.params.id;
    const [user, setUser] = useState({});
    useEffect(()=>{
        getUser(userId, setUser);
    }, []);
    return (
        <React.Fragment>
            <div>User id: {userId}</div>
            <div>Имя: {user.fullName}</div>
            <div>Почта: {user.email}</div>
            <div>Адрес: {user.address}</div>
        </React.Fragment>
    )
};

export default UserPage;
