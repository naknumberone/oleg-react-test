import React, {useEffect, useState} from 'react';
import {getUsers, createUser, removeUser} from "../../services/UserService";
import List from "../../components/List/List";
import UserForm from "../../components/UserForm/UserForm";

const Users = (props) => {
    const [users, setUsers] = useState([]);
    const handleSubmit = async (event) => {
        event.preventDefault();
        const {
            fullName,
            email,
            address
        } = event.target.elements;
        const newUser = await createUser({
            fullName: fullName.value,
            email: email.value,
            address: address.value
        });
        setUsers([...users, newUser]);
    };
    const handleRemove = async (userId) => {
        await removeUser(userId);
        setUsers(users.filter(user => user._id !== userId));
    };
    useEffect(() => {
        getUsers(setUsers)
    }, []);
    return (
        <div>
            <List items={users} emptyText={'Ещё нет созданных юзеров :)'} handleRemove={handleRemove} />
            <UserForm handleSubmit={handleSubmit} buttonText={'Создать пользователя'} />
        </div>
    );
};

export default Users;
